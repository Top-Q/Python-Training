from exersize5.Card import Card
import random


class Deck:

    _cards_list = []

    def __init__(self):
        suit_types = {"Hearts", "Diamonds", "Clubs", "Pikes"}
        for type in suit_types:
            for i in range(1, 14):
                self._cards_list.append(Card(type, i))

    def cards_amount(self):
        return len(self._cards_list)

    def shuffle(self):
        random.shuffle(self._cards_list)

    def get_x_cards(self, cards_amount):
        sliced_cards = self._cards_list[0: cards_amount]
        del self._cards_list[0: cards_amount]
        return sliced_cards