class Card:

    def __init__(self, suit, number):
        self._suit = suit
        self._number = number

    def get_card_number(self):
        return self._number

    def get_card_suit(self):
        return self._suit

    def __str__(self):
        return self.get_card_suit() + " " + str(self.get_card_number())