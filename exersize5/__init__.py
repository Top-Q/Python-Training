import sys
from Card import Card

from Deck import Deck
from Hand import Hand

def main(argv):
    deck = Deck()
    deck.shuffle()
    player_one_hand = Hand(deck.get_x_cards(26))
    player_two_hand = Hand(deck.get_x_cards(26))
    game_round = 1
    while player_one_hand.cards_amount() and player_two_hand.cards_amount():
        print "game round #" + str(game_round)
        player_one_card = player_one_hand.reveal_top()
        player_two_card = player_two_hand.reveal_top()
        print "player one drew: " + str(player_one_card)
        print "player two drew: " + str(player_two_card)
        if player_one_card.get_card_number() > player_two_card.get_card_number():
            player_one_hand.add_new_cards([player_one_card, player_two_card])
            player_one_hand.shuffle()
        elif player_one_card.get_card_number() < player_two_card.get_card_number():
            player_two_hand.add_new_cards([player_one_card, player_two_card])
            player_two_hand.shuffle()
        print "------------------------"
        game_round += 1
        if not player_one_hand.cards_amount():
            print "player two wins"
        if not player_two_hand.cards_amount():
            print "player one wins"


if __name__ == "__main__":
    main(sys.argv)