import random

class Hand:

    def __init__(self, cards_list):
        self._cards_list = cards_list

    def reveal_top(self):
        if len(self._cards_list) > 0:
            return self._cards_list.pop(0)

    def add_new_cards(self, cards_list):
        self._cards_list.extend(cards_list)

    def cards_amount(self):
        return len(self._cards_list)

    def shuffle(self):
        random.shuffle(self._cards_list)