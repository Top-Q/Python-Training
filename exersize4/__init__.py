import sys
from random import randint

def main(argv):
    bf = "++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>."
    data = [0]*100

    inst_index = 0
    cur = 0
    run = True
    loop_start = 0
    jump_forward = False
    while run:
        cur_inst = bf[inst_index]
        if not jump_forward:
            if cur_inst == '+':
                data[cur] += 1
            if cur_inst == '-':
                data[cur] -= 1
            if cur_inst == '.':
                sys.stdout.write(chr(data[cur]))
            if cur_inst == ',':
                input(data[cur])
            if cur_inst == '[':
                if data[cur] != 0:
                    loop_start = inst_index
                else:
                    jump_forward = True
            if cur_inst == ']':
                if data[cur] != 0:
                    inst_index = loop_start
                else:
                    jump_forward = False
            if cur_inst == '<':
                cur -= 1
            if cur_inst == '>':
                cur += 1
        elif cur_inst == ']':
            jump_forward = False
        inst_index += 1
        if inst_index >= len(bf):
            run = False

if __name__ == "__main__":
    main(sys.argv)