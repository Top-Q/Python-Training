import sys

def main(argv):
    str1 = "The weather is awesome today"
    str2 = "do it"
    if len(str1) >= len(str2):
        long_text = str1
        short_text = str2
    else:
        long_text = str2
        short_text = str1
    counter = 0
    for letter in short_text:
        if letter in long_text:
            counter += 1
    if counter == len(short_text):
        print "true"
    else:
        print "false"

if __name__ == "__main__":
    main(sys.argv)