import sys

def main(argv):
    for i in range(11):
        temp = ""
        for j in range(1, 12):
            current_num = i * 10 + j
            if current_num != 111:
                is_coze_waza_loza = current_num % 3 == 0 or current_num % 5 == 0 or current_num % 7 == 0
                if is_coze_waza_loza:
                    if current_num % 3 == 0:
                        temp += "Coza"
                    if current_num % 5 == 0:
                        temp += "Loza"
                    if current_num % 7 == 0:
                        temp += "Waza"
                    temp += " "
                else:
                    temp += str(current_num) + " "
        print temp + "\n"

if __name__ == "__main__":
    main(sys.argv)